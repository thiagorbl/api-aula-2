const express = require('express'); // precisa do express e precisa do cors
const cors = require('cors');
const app = express(); // cria uma aplicação do express
// require('./src/Routes/index')(app); // deixou como comentario pra usar depois
app.use(cors());
app.use(express.json());
app.listen(3333); // server que ele vai utilizar "escutar"